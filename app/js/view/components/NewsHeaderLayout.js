import React from "react";

export default class NewsHeaderLayout extends React.Component {
	render(){
		return(
			<div id="mppf-news-header" class="row">
				<div id="title-img" class="medium-3 columns text-center">
					<img src="image/test/mppf_with_white_bg.png"></img>
				</div>
				<div class="medium-9 columns">
					<p id="title-text">
						<span class="news-title">{this.props.title}</span>
						<br/>
						<span class="news-post-date">最後更新：{this.props.lastUpdate}</span>
					</p>
				</div>
			</div>
		);
	}
}