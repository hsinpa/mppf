import React from "react";
import { Link, Route } from 'react-router';

export default class NewsFeed extends React.Component {
  componentDidMount() {
    if (typeof(FB) != 'undefined') {
      FB.XFBML.parse($('#mppf-news-feeds')[0]);
      console.log("Call");
    }
}
  //
  // <div id="fb-root"></div>
  render() {
    return (
          <div id="mppf-news-feeds">

            <ul>
              <li><h4>最新消息</h4></li>
              <li><div class="row">
                <img src="image/test/mppf_with_white_bg.png" class="small-4 columns"></img>
                <section  class="small-8 columns">
                  <Link to="recentNews/PDMO"><h4>海事運作專業文憑</h4></Link>
                  <p class="news-feed-content">海事訓練學院現推出兼讀制「海事運作專業文憑」(PDMO) 課程，讓學員可獲取更高學歷，來提升個人職涯發展的競爭力。</p>
                </section>
              </div></li>

              <li><div class="row">
                <img src="image/test/mppf_with_white_bg.png" class="small-4 columns"></img>
                <section  class="small-8 columns">
                  <Link to="recentNews/QuestionCompetition"><h4>「海上的奇遇」徵文比賽</h4></Link>
                  <p class="news-feed-content">主題：以「海上的奇遇」為題，自由發揮，於文中發表個人見解、記敘難忘經歷、也可以表達渴望、書寫暢想</p>
                </section>
              </div></li>


              <li><div>
                <section class="fb-container">
                    <div class="fb-page" data-href="https://www.facebook.com/seagoinghk/"
                       data-tabs="timeline" data-height="250" data-small-header="true"
                        data-adapt-container-width="true" data-hide-cover="false"
                        data-show-facepile="true">
                        <div class="fb-xfbml-parse-ignore">
                          <blockquote cite="https://www.facebook.com/seagoinghk/">
                            <a href="https://www.facebook.com/seagoinghk/">海事專才推廣聯盟</a>
                          </blockquote>
                        </div>
                      </div>
                </section>
              </div></li>
            </ul>
          </div>
    );
  }
}
