import React from "react";
import StoryModal from "./StoryModal";
import { Link, Route } from 'react-router';

export default class NaviStories extends React.Component {

    componentDidMount() {
        $(document).foundation();
    }

    render() {
        return (
            <div id="mppf-naviStory" class="row">

                <h1>航海故事</h1>
                <article>
                    <div class="medium-4 large-3 columns">
                        <Link to="/story/Carmen">
                            <section>

                                <img src="image/people/carmen.jpg"></img>
                                <h5 class="story-title">陳嘉敏船長</h5>


                            </section>
                        </Link>
                    </div>





                    <div class="medium-4 large-3 columns">
                        <Link to="/story/Lam_Ming_Fung">
                            <section>

                                <img src="image/people/Lam_Ming_Fung_sq.jpg"></img>
                                <h5 class="story-title">林銘鋒船長</h5>


                            </section>
                        </Link>
                    </div>

                    <div class="medium-4 large-3 columns">
                        <Link to="/story/lyl">
                            <section>

                                <img src="image/people/Matt_sq.jpg"></img>
                                <h5 class="story-title">黎永麟船長</h5>


                            </section>
                        </Link>
                    </div>


                    <div class="medium-4 large-3 columns">
                        <Link to="/story/Matthew">
                            <section>

                                <img src="image/people/Matthew_sq.png"></img>
                                <h5 class="story-title">蕭邦泰驗船主任(輪機) </h5>


                            </section>
                        </Link>
                    </div>





                </article>

                <StoryModal />
            </div>
        );
    }
}
