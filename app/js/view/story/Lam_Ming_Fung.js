import React from "react";

export default class StoryLamMingFungPage extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-story-lam-ming-fung">

        <div class="row">
        <div class="columes">

<td width="725" class="cnormal"><h1>林銘鋒船長</h1>
      <p class="style2">公司：巴拉歌船務公司 (Parakou Shipping Ltd.)<br/>職位：副總裁 (Vice President)</p>
      <p class="style2">1998年我在理工大學航運系畢業後，因為老師和前輩們的建議，我決定投身航海事業。
	  當時我給自己訂下一個十年計劃，十年之內最起碼要考到船長證書和有至少一年的大副經驗。
	  </p><p></p>當時我加入了華光船務公司當甲板學習生，累積了13個月的海齡，符合應考香港海事處二副證書的要求。在這13個月裡，我從一個甚麼都不懂的小伙子，
	  學習到刻苦耐勞、成熟、虛心學習和能夠獨立思考。在2000年，我考到人生第一張專業證書--二副證書(Class 3 (Deck) Certificate of Competency) 。
	  <p></p>在2000年12月，我再上船繼續積累海齡，當時我的職位已被提升為三副。經過11個月的工作，得到船長的認同和肯定，我在2001年11月被提升為二副。
	  在這艘船上，我足足工作了19個月，有足夠的海齡應考大副證書。當時我在考慮在那裡考大副，因香港當年已沒有培訓課程，所以我決定去英國考大副證書。
	  在2002年9月，我去了英國Southampton的Warsash Maritime Centre就讀船長/大副綜合課程。
	  在2003年6月，我考到了英國海事當局(MCA)發出的大副證書(Class 2 (Deck) Certificate of Competency)。
	  <p></p>在2003年9月，我再次上船累積海齡，向著我的目標﹣船長證書﹣前進和努力。到2005年7月，我累積了足夠的海齡應考船長證書。
	  在2006年1月，我終於考到了英國海事當局(MCA)發出的船長證書(Class 1 (Deck) Certificate of Competency)。
	  <p></p>在2006年2月，我被公司提升為大副到船上工作。累積了13個月的大副經驗後，我得到兩位船長的認同和肯定，他們並向公司推薦我有能力提升為船長。
	  在2007年4月，我終於被提升為船長。我的十年計劃基本上完滿達成。
	  <p></p>因此在2007年12月上岸後，我決定結束我的航海生涯，在岸上找工作。我跟很多老師和前輩請教，到底那一份工作才是最好？
	  當時有三份工作給我考慮，一是香港海事處的海事主任或驗船主任、二是領航員、三是船公司的總船長(Port Captain)。經過多番考慮，我最終選擇了在船公司當總船長。
	  因我認為在船公司工作更能發揮我的專業和多年的經驗，當然其他兩份工作也是非常好和有前途的。
	  <p></p>在2008年1月，我選擇了加入巴拉歌船務當總船長一職。現在，我在這公司工作了已有四年多，我學習了怎樣管理船隊、管理船員、危機處理和應變，得到很大的成就感。
	  在這四年裡，我不斷的提升自己的能力，也從總船長一步一步提升到副總裁一職。
	  <p></p>現在我可以肯定的向大家說“我沒有入錯行，行船真的是一個不錯的行業”。當你拿到專業資格和相應的航海經驗，對未來的工作上有很大漸帠B和幫助。航運是一個不會沒落的行業，
	  業界內極其需要有航海經驗的專業船員在岸上擔當不同的崗位，例如：政府機關、港口管理、船舶管理、船舶經紀、船舶保險，港內領航、修船廠、船級社等等．．．機會就在你們面前！
<p></p>
<p></p>
    </td>


        </div>
         </div> 
         </div>
    );
  }
}