import React from "react";

export default class StorylylPage extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-story-lyl">



<div class="row">
        <div class="columes">
<td width="725" class="cnormal">
	<h1>黎永麟船長</h1>
      <p class="style2">跟大部分香港年青人一樣，我都經歷過會考，聯招，考試，畢業，面對社會的焦慮。
走到今日，總算找到自己的尊業，自己的方向。藉此機會與各位分享一條不一樣的路，希望給大家多一個選擇!
</p><p></p>繼應付過九三年中學會考和九五年的高級程度會考，選科是一個人生的大決擇。正藉當時八號貨櫃碼頭的運作日趨飽和和九號貨櫃碼頭的極速上馬，
心想航運巿?必然大有可為，故決定選讀當時理工大學全新開辦的航運科技及管理學學士課程。三年的學習令我意識到航運並非夕陽行業，反而是國際貿易的重要一環。
<p></p>畢業後本想從事航運物流管理工作，但隨著對業介的認識和一些師兄的指引，發現航海技術人材的渴求更為欣切，而且這些技術，
經驗和知識在管理港口和航運公司都有廣泛的發揮機會，故毅然決定畢業後去"行船"!
<p></p>由九八年至零七年，九年的行船過程，實質有五年多在船上生活，這些日子，苦樂參半。想念家人朋友的感受難以言喻；
唯樂在工作的滿足感和見識這個世界的經歷，現在回想起來，百般滋味在心頭。曾經有人問過我到過甚麼地方，一數之下原來我到過二十多個國家，
五十多個地方，還至少圍繞地球航行五週!<p></p>工作上我擔當過三副，二副和大副，各有不同的角色和責任。每升一級，除了肩膀上背多"一條柴"外，
還意味著自己對船員和海上安全的承擔。茫茫大海，除了自己，沒有其他支援，每一個正確的決定都是關鍵，工作的滿足感正正由自己獨當一面的時候產生。
<p></p>除了海上工作，休假時都要應付大大小小的考核和訓練，當中最難忘的經歷可算是留學英國修咸頓的一年，修讀航海技術課程，考當地的公開試和尊業試。
除了獲得尊業的知識，船長的尊業資格，留學時的體驗給我留下深刻的回憶!
<p></p>海上的生活有規律，有挑戰性，還有豐厚的回報，但畢竟這裏都只是"跳板"，岸上發展始終都是我的最終目標。
<p></p>零七年，離開海上的生活，回港後，我考慮過當水警訓練學校督察（航海），海事處海事主任，航運管理公司船舶主管和香港領港會領航員。
以上每個崗位都有優厚的代遇和前景，但因為青黃不接的緣故，不少職級都找不到合資格的人選而懸空，而我考慮到自己的性格，最後還是選擇了當領航員。
<p></p>領航員的工作有點兒像"代客泊車"，分別在於領航員要駕馭的是長逾二百米，三百米，甚至四百米的貨船，郵輪或油輪在香港狹窄的水域航行。
這種駕馭龐然大物的感覺甚具挑戰性，而且有效率的領航工作亦助長港口的發展和運作，總算是香港航運業的小小一員！
<p></p>社會上有好多人說現今年輕一代機會少了！我不敢妄然否定，但當機會來臨的時候，是不是每位年青人都捉得住呢？
<p></p>"行船"並非別無他選的決擇，恰恰相反，還算是一塊不錯的"跳板", 讓年輕人獲得一門受社會認可的尊業，最重要還是將來有無可估量的發展空間。
正因為這個原因，過去十年，港大和理大的畢業生選擇去"行船"的不少於四十人；IVE畢業後入行的更超過百人。他們都是看準這個機遇，敢於接受挑戰的一群。
<p></p>社會上對"行船"這個行業有不少晤解，全因為這並非主流行業或對這個行業了解不多所致。
<p></p>如果你有興趣對這個行業有更深入了解，不妨跟我們的推廣聯盟成員聯絡，我們可提供更多資料給你參考！
<p></p>祝願各年青人都找到自己的方向，乘著機遇，發展自己的事業。
<p></p>黎永麟<br/>2012年
<br/>
<br/>
	<p>2007 船上的辦公室<br/><img src="image/people/Matt01.jpg"/></p><br/>
	<p>2005 澳洲訪客登船參觀時合照<br/><img src="image/people/Matt02.jpg"/></p><br/>
	<p>2002 英國修咸頓留學時 "長駐" 的圖書館留影<br/><img src="image/people/Matt03.jpg"/></p><br/>
	<p>2004 韓國造船厰留影，單從螺旋槳的大小，可想象船体的份量<br/><img src="image/people/Matt04.jpg"/></p><br/>
	<p>2005 新船下水禮留影<br/><img src="image/people/Matt05.jpg"/></p><br/>
	<p>2008 我的 "海上法拉利"<br/><img src="image/people/Matt06.jpg"/></p>
	<p></p>
    </td>
</div> 
         </div>
    </div>
    );
  }
}