import React from "react";

export default class StoryMatthewPage extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-story-matthew">


<div class="row">
        <div class="columes">
<td width="714">
      <p><strong>海上生活~輪機師</strong></p>
      <p align="justify">在職業展覽或講座時，常常有人問我以下的問題:
<br/>一個正在學習機械工程的學生應如何選擇他們未來的職業道路？
<br/>為什麼要選擇在海上工作? 在海上工作有甚麼好處
 
 </p><p></p>
 <table>

<caption></caption>
<tbody><tr>
       <td><img src="image/people/LifeOfEngin01.png"/></td>
       <td><img src="image/people/LifeOfEngin02.png"/></td>
</tr>
</tbody></table>
       <br/><p></p>
      <p align="justify">香港的航運業裡有不同的組職，例如：船公司、船級社、海事處和船塢等。他們都需要大量有航海經驗的輪機師以填補其高級海員，驗船師和船務經理。
      <br/></p>
 <p><img src="image/people/LifeOfEngin03.png"/></p>
      <p align="justify">在海上工作會確保有好的收入及進升。
<br/>很多從機械工程畢業的學生 在岸上都很難找到很好的工作， 
<br/>亦有很多人需要返回內地， 也有不少發現工作沒有進升的機會。
在海上工作不但有很好的收入，更有明確的進升途徑。
<br/>當有足夠的工作經驗及考得資格後，更會得到岸上管理層的就業機會
<br/>要成為特許工程師，在海上工作也是其中一種途徑。
</p>
 <p align="justify">在海上工作富有樂趣及挑戰性。你可以到不同的地方，喻工作於娛樂。這是在香港很多人夢想的工作。
 
 </p><p></p>
 <table>

<tbody><tr>
       <td><img src="image/people/LifeOfEngin04.png"/></td>
       <td><img src="image/people/LifeOfEngin05.png"/></td>
</tr>
</tbody></table>

<p align="justify">在海上工作十分有滿足感，因為你必須懂得維修所有種類的機械，如主機，發電機，鍋爐，淨化器及泵等，。
 
 </p><p></p>
 <table>

<tbody><tr>
       <td><img src="image/people/LifeOfEngin06.png"/></td>
       <td><img src="image/people/LifeOfEngin07.png"/></td>
</tr>
</tbody></table>

 <p><img src="image/people/LifeOfEngin08.png"/></p>
 </td>

</div>
         </div> 
 </div>
    );
  }
}