import React from "react";

export default class Resources extends React.Component {
    componentDidMount() {
        $(document).foundation();
    }

    render() {
        return (
            <div id="mppf-resources" class="row">
                <h1>考試預備資源</h1>
                <article>

                    <p>所有資源只供學習之用，不可用作商業用途。如閣下發現有檔案侵犯版權，請聯絡我們，我們會盡快刪除該檔案。此外，所有上載之資料只屬自願性質，本網站並不保證這些資料屬最新版本，學生應與海事處及其他有關學院查證，或於考試前報讀備試課程，本網站絕不會就這些資料負任何責任。</p>
                    <br/>
                    <ul class="tabs" data-tabs id="example-tabs">
                        <li class="tabs-title is-active"><a href="#classone" aria-selected="true">Class 1 (Master Mariner) </a></li>
                        <li class="tabs-title"><a href="#classthree1">Class 3 (Deck Officer) </a></li>
                        <li class="tabs-title"><a href="#classthree2">Class 3 (Engineer Officer) </a></li>
                    </ul>


                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel is-active" id="classone">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tbody><tr valign="BOTTOM">
                                    <td width="170">
                                        <p styles="border: none; padding: 0in"><a name="table1"></a><strong><span lang="en-US">Subject</span></strong></p>
                                    </td>
                                    <td width="105">
                                        <p align="CENTER" styles="border: none; padding: 0in"><strong><span lang="en-US">Session</span></strong></p>
                                    </td>
                                    <td width="105">
                                        <p align="CENTER" styles="border: none; padding: 0in"><strong><span lang="en-US">Date</span></strong></p>
                                    </td>
                                    <td colspan="2" width="165">
                                        <p styles="border: none; padding: 0in">&nbsp; </p>
                                    </td>
                                    <td width="149">
                                        <p styles="border: none; padding: 0in">&nbsp; </p>
                                    </td>
                                </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">Shipboard Operations</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1989/90</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/89_90(1).pdf"><span lang="en-US">22-May-90</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/89_90(2).pdf"><span lang="en-US">11-Jun-90</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1990/91</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/90_91(1).pdf"><span lang="en-US">11-Jun-91</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/90_91(2).pdf"><span lang="en-US">24-Jun-91</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1991/92</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/91_92(1).pdf"><span lang="en-US">9-Jun-92</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1992/93</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/92_93(1).pdf"><span lang="en-US">7-Jun-93</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1993/94</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/93_94(1).pdf"><span lang="en-US">7-Jun-94</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1994/95</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/94_95(1).pdf"><span lang="en-US">6-Jun-95</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1996/97</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/shipboard_operations/96_97(1).pdf"><span lang="en-US">28-May-97</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">Commerce and Law</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1989/90</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/89_90(1).pdf"><span lang="en-US">23-May-90</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/89_90(2).pdf"><span lang="en-US">11-Jun-90</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1990/91</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/90_91(1).pdf"><span lang="en-US">12-Jun-91</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/90_91(2).pdf"><span lang="en-US">24-Jun-91</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1991/92</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/91_92(1).pdf"><span lang="en-US">10-Jun-92</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1992/93</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/92_93(1).pdf"><span lang="en-US">9-Jun-93</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/92_93(2).pdf"><span lang="en-US">28-Jun-08</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1993/94</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/93_94(1).pdf"><span lang="en-US">8-Jun-94</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/93_94(2).pdf"><span lang="en-US">27-Jun-94</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1994/95</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/94_95(1).pdf"><span lang="en-US">5-Jun-95</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/94_95(2).pdf"><span lang="en-US">26-Jun-95</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1995/96</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/95_96(1).pdf"><span lang="en-US">10-Jun-96</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/commerce_and_law/95_96(1)_marking_scheme.pdf"><span lang="en-US">(marking
                                                scheme) </span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">Ship Technology</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1989/90</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/89_90(1).pdf"><span lang="en-US">31-May-90</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1990/91</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/90_91(1).pdf"><span lang="en-US">10-Jun-91</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/90_91(2).pdf"><span lang="en-US">24-Jun-91</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1991/92</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/91_92(1).pdf"><span lang="en-US">8-Jun-92</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1992/93</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/92_93(1).pdf"><span lang="en-US">8-Jun-93</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1994/95</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/94_95(1).pdf"><span lang="en-US">7-Jun-95</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1995/96</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/95_96(1).pdf"><span lang="en-US">12-Jun-96</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1996/97</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/96_97(1).pdf"><span lang="en-US">26-May-97</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/ship_technology/96_97(1)_marking_scheme.pdf"><span lang="en-US">(marking
                                                scheme) </span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">Passage Planning</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1991/92</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/91_92(1).pdf"><span lang="en-US">24-Dec-91</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1993/94</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/93_94(1).pdf"><span lang="en-US">14-Dec-93</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1994/95</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/94_95(1).pdf"><span lang="en-US">13-Dec-94</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1995/96</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/95_96(1).pdf"><span lang="en-US">12-Dec-95</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/95_96(2).pdf"><span lang="en-US">(3
                                                additional papers) </span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1996/97</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/96_97(1).pdf"><span lang="en-US">18-Dec-96</span></a></p>
                                        </td>
                                        <td width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/96_97(1)_marking_scheme.pdf"><span lang="en-US">(marking
                                                scheme) </span></a></p>
                                        </td>
                                        <td colspan="2" width="149">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/passage_planning/96_97(2).pdf"><span lang="en-US">3-Jan-97</span></a></p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">Navigation</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1991/92</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/91_92(1).pdf"><span lang="en-US">20-Jan-92</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1992/93</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/92_93(1).pdf"><span lang="en-US">14-Dec-92</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/92_93(2).pdf"><span lang="en-US">14-Dec-92</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/92_93(3).pdf"><span lang="en-US">(1
                                                additional paper) </span></a></p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1993/94</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/93_94(1).pdf"><span lang="en-US">13-Dec-93</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/93_94(2).pdf"><span lang="en-US">10-Jan-94</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1995/96</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/95_96(1).pdf"><span lang="en-US">11-Dec-95</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/95_96(2).pdf"><span lang="en-US">16-Jan-96</span></a></p>
                                        </td>
                                        <td width="149">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/95_96(3).pdf"><span lang="en-US">29-Jan-96</span></a></p>
                                        </td>
                                    </tr>
                                    <tr valign="BOTTOM">
                                        <td width="170">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in">1996/97</p>
                                        </td>
                                        <td width="105">
                                            <p align="CENTER" styles="border: none; padding: 0in"><a href="/pdf/ClassOne/Past_Paper/navigation/96_97(1).pdf"><span lang="en-US">16-Dec-96</span></a></p>
                                        </td>
                                        <td colspan="2" width="165">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                        <td width="149">
                                            <p styles="border: none; padding: 0in">&nbsp; </p>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel" id="classthree1">
                            <table width="100%" border="0" cellpadding="0" cellspacing="3">
                                <tbody><tr>
                                    <td>
                                        <p>IMO Resolutions: </p>
                                        <p><a a href="/pdf/ClassThree/IMO/A224VII.pdf"><span lang="en-US">IMO
                                            Resolution A.224(VII) PERFORMANCE STANDARDS FOR ECHO-SOUNDING
                                            EQUIPMENT</span></a></p>
                                        <p><span lang="en-US"><a a href="/pdf/ClassThree/IMO/A817(19)_ECDIS.pdf">IMO
                                            Resolution A.817(19) PERFORMANCE STANDARDS FOR ELECTRONIC CHART
                                            DISPLAY AND INFORMATION SYSTEMS (ECDIS) </a> </span>
                                        </p>
                                        <p><a a href="/pdf/ClassThree/IMO/A824(19).pdf"><span lang="en-US">IMO
                                            Resolution A.824(19) PERFORMANCE STANDARDS FOR DEVICES TO
                                            INDICATE SPEED AND DISTANCE</span></a></p>
                                        <p><span lang="en-US"><a a href="/pdf/ClassThree/IMO/A893(21)_voyplan.pdf">IMO
                                            Resolution A.892(21) GUIDELINES FOR VOYAGE PLANNING</a> </span>
                                        </p>
                                        <p><span lang="en-US"><a a href="/pdf/ClassThree/IMO/A917(22).pdf">IMO
                                            Resolution A.917(22) GUIDELINES FOR THE ONBOARD OPERATIONAL USE
                                            OF SHIPBORNE AUTOMATIC IDENTIFICATION SYSTEMS (AIS) </a> </span>
                                        </p>
                                        <p><a a href="/pdf/ClassThree/IMO/A918(22).pdf"><span lang="en-US">IMO
                                            Resolution A.918(22) IMO STANDARD MARINE COMMUNICATION PHRASES</span></a></p>
                                        <p><span lang="en-US"><a a href="/pdf/ClassThree/IMO/MSC64(67)Annex4.pdf">IMO
                                            Resolution MSC.64(67) Annex 4 RECOMMENDATION ON PERFORMANCE
                                            STANDARDS FOR RADAR EQUIPMENT</a> </span>
                                        </p>
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <p>IALA: </p>
                                            <p><a a href="/pdf/ClassThree/IALA/IALA_Bouyage.pdf"><span lang="en-US">IALA
                                                Bouyage System</span></a></p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/IALA/AIS_GUIDE.pdf">IALA
                                                GUIDELINES ON UNIVERSAL SHIPBORNE AUTOMATIC IDENTIFICATION
                                                SYSTEM (AIS) </a> </span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Others: </p>
                                            <p><a a href="/pdf/ClassThree/Others/Mariners_Handbook.pdf"><span lang="en-US">Mariners'
                                                Handbook</span></a></p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/GPSBasics_Leica.pdf">Introduction
                                                to GPS by Leica</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/Chapter_13.pdf">Introduction
                                                to GPS (Chinese) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/seaways.pdf">Useful
                                                Articles extracted from SEAWAYS</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/US_Chart_Symbols.pdf">US
                                                Chart 1 (Symbols) </a> </span>
                                            </p>
                                            <p><a a href="/pdf/ClassThree/Others/STCW95.doc"><span lang="en-US">STCW
                                                95 - Watchkeeping At Sea</span></a></p>
                                            <p><a a href="/pdf/ClassThree/Others/2.1_Lifebuoys.txt"><span lang="en-US">Lifebuoys
                                            </span></a>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/4_4_General_reqs_lifeboats.txt">General
                                                requirements for lifeboats</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/7_1_Line-throwing_appliances.txt">Line-throwing
                                                appliances</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/Solas_Reg19.pdf">Solas
                                                Regulation 19 Carriage requirements for shipborne navigational
                                                systems and equipment</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Others/derrick.pdf">Derrick
                                                - Calculaions</a> </span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Past Papers and Possible Questions: </p>
                                            <p><a href="http://www.seagoinghk.org/ClassThree/Past_Papers/2010July_Class3_Seagoing_Examination.doc">2010
                                                July Class 3 Seagoing Examination</a></p>
                                            <p><a href="http://www.seagoinghk.org/ClassThree/Past_Papers/2010%20March%20COC%20exam.doc"><span lang="en-US">2010
                                                March Class 3 Seagoing Examination</span></a></p>
                                            <p><a href="http://www.seagoinghk.org/ClassThree/Past_Papers/Sept_2009_oral_exam.doc"><span lang="en-US">2009
                                                September Oral Exam</span></a></p>
                                            <p><a href="http://www.seagoinghk.org/ClassThree/Past_Papers/2009Mar_Class3_Seagoing_Examination.doc"><span lang="en-US">2009
                                                March Class 3 Seagoing Examination</span></a></p>
                                            <p><a href="http://www.seagoinghk.org/ClassThree/Past_Papers/2008Nov_Class3_Seagoing_Examination.doc"><span lang="en-US">2008
                                                November Class 3 Seagoing Examination</span></a></p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/2008written.zip">2008
                                                Written Exam</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Exam_Proficiency_Communications_(signals)_071026.doc">2007.10.26
                                                Exam for Proficiency in Communications (signals) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Exam_Proficiency_Communications_(signals)_070928.doc">2007.9.28
                                                Exam for Proficiency in Communications (signals) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Exam_0707.zip">2007
                                                Written Exam</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/2006_Written_exam.doc">2006
                                                Written Exam</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/2006_Oral.doc">2006
                                                Oral Exam</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Ocean_Nav_paper.doc">Ocean
                                                Navigation</a> </span>
                                            </p>
                                            <p><a href="../../Chinese/Past_Papers/past%20paper.zip"><span lang="en-US">PolyU
                                                Past Paper </span></a>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Watchkeeping_paper.doc">Watchkeeping
                                                Paper Questions</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/C3_Oral_Qs.doc">Oral
                                                Questions</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/ISM_Qs.doc">ISM
                                                FAQ</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Navigation_Oral1.doc">Navigation
                                                Q&amp; A</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Navigation.doc">Navigation
                                            (Multiple Choice) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/Safety.doc">Safety
                                            (Multiple Choice) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/COLREG_OOW_Q.doc">COLREG
                                                Q&amp; A</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Past_Papers/COLREG.doc">COLREG
                                            (Multiple Choice) </a> </span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Examination Syllabuses with Useful Notes: </p>
                                            <p>Part A</p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Syllabus/Part_A/A1_Coastal_Nav.doc">Paper
                                                1 - Coastal Navigation</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Syllabus/Part_A/A2_Offshore_Nav.doc">Paper
                                                2 - Offshore Navigation</a> </span>
                                            </p>
                                            <p>Part B</p>
                                            <p><span lang="en-US">Paper 1 - <a a href="/pdf/ClassThree/Syllabus/Part_B/B1a_Ship_Cons.doc">Ship
                                                Construction</a> <a a href="/pdf/ClassThree/Syllabus/Part_B/B1b_Stability.doc">Stability</a>
                                                <a a href="/pdf/ClassThree/Syllabus/Part_B/B1c_Cargo_handle_stow.doc">Cargo
                                                    handling and stowage</a> </span>
                                            </p>
                                            <p><span lang="en-US">Paper 2 - <a a href="/pdf/ClassThree/Syllabus/Part_B/B2_Watchkeeping_Meteorology.doc">Watchkeeping
                                                and Meteorology</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Syllabus/Part_B/B2a_Watchkeeping_SAR.doc">Watchkeeping
                                                &amp; Search And Rescue</a> <a a href="/pdf/ClassThree/Syllabus/Part_B/B2b_Ship_Operation.doc">Ship
                                                    Operation</a> <a a href="/pdf/ClassThree/Syllabus/Part_B/B2c_Meteorology.doc">Meteorology</a>
                                            </span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Other Notes: </p>
                                            <p><a a href="/pdf/ClassThree/Other_Notes/Summary_Ocean_FFA.xls"><span lang="en-US">Summary
                                                Notes </span></a>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Other_Notes/Naviagtional_Watch_03.bmp">Taking
                                                over a Naviagtional Watch</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Other_Notes/Pilot_Boarding.jpg">Pilot
                                                Boarding Arrangement</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Other_Notes/Deviation_Table_03.bmp">Deviation
                                                Table</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Other_Notes/garbage_management.jpg">Garbage
                                                Management</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Other_Notes/Life_saving.pdf">Life-saving
                                                Signals</a> </span>
                                            </p>
                                            <p><a a href="/pdf/ClassThree/Other_Notes/Cap%20478T%20sch%201%20part%203.doc"><span lang="en-US">Cap
                                                478T – Merchant Shipping (Seafarers) (Certification and
                                                Watchkeeping) Regulation Schedule 1 Part 3: Principles to be
                                                observed in keeping a navigational watch</span></a></p>
                                            <p><a a href="/pdf/ClassThree/Other_Notes/mgn_315_amended_2_nav%20watch.pdf"><span lang="en-US">Marine
                                                Guidance Note - MGN 315 (M): Keeping a safe navigational watch
                                                on merchant vessels</span></a></p>
                                            <p><a a href="/pdf/ClassThree/Other_Notes/Safety%20of%20navigation.pdf"><span lang="en-US">Safety
                                                of Navigation</span></a></p>
                                            <p><a a href="/pdf/ClassThree/Other_Notes/Unit%202%20Bridge%20Procedures.pdf"><span lang="en-US">Bridge
                                                Procedures</span></a></p>
                                            <p>&nbsp; </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Programs</p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/calculators.zip">Nautical
                                                Calculators</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/Nav_Rules.zip">Nav
                                                Rules</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/Star_Calc.zip">Star
                                                Calc</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/Examiner.zip">Examiner</a>
                                            </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/DeckReviewer.zip">Deck
                                                Reviewer</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/Distance_Tables.zip">Distance
                                                Tables</a> </span>
                                            </p>
                                            <p><span lang="en-US"><a a href="/pdf/ClassThree/Programs/JustLearnMorseCode.msi">Morse
                                                Code</a> </span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Useful Links: </p>
                                            <p><span lang="en-US"><a href="http://www.landfallnavigation.com/hydrostatic.html">Hydrostatic
                                                Release Unit (HRU) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a href="http://www.navcen.uscg.gov/marcomms/gmdss/epirb.htm">Emergency
                                                Position Indicating Radiobeacon (EPIRB) </a> </span>
                                            </p>
                                            <p><span lang="en-US"><a href="http://ww2010.atmos.uiuc.edu/(Gh)/guides/mtr/home.rxml">Meteorology</a>
                                            </span>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="tabs-content" data-tabs-content="example-tabs">
                        <div class="tabs-panel" id="classthree2">
                            <td>
                                <p class="msonormal" styles="border: none; padding: 0in"><a href="http://www.seagoinghk.org/Engineer/ClassThree/Past_Paper/q1.doc"><span lang="en-US">Question
                                    and Answer 1</span></a></p>
                                <p class="msonormal" styles="border: none; padding: 0in"><a href="http://www.seagoinghk.org/Engineer/ClassThree/Past_Paper/q2.doc"><span lang="en-US">Question
                                    and Answer 2</span></a></p>
                                <p class="msonormal" styles="border: none; padding: 0in"><a href="http://www.seagoinghk.org/Engineer/ClassThree/Past_Paper/q3.doc"><span lang="en-US">Question
                                    3 (Oral) </span></a></p>
                                <p class="msonormal" styles="border: none; padding: 0in"><a href="http://www.seagoinghk.org/Engineer/ClassThree/Past_Paper/q4.doc"><span lang="en-US">Question
                                    4 (Oral) </span></a></p>
                                <p class="msonormal" styles="border: none; padding: 0in"><a href="http://www.seagoinghk.org/Engineer/ClassThree/Past_Paper/q5.doc"><span lang="en-US">Question
                                    5 (Oral) </span></a></p>
                            </td>
                        </div>
                    </div>


                </article>
            </div>
        );
    }
}
