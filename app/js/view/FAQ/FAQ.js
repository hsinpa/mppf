import React from "react";

export default class Home extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-faq">
        <h1>常見問題</h1>

        <div class="row">
          <ul class="accordion" data-accordion>
            <li class="accordion-item is-active" data-accordion-item>
              <a href="#" class="accordion-title">為什麼我要到海上工作去？</a>
              <div class="accordion-content" data-tab-content>
                我剛中學畢業，未能取得入讀大學的理想成績，但仍想繼續升學，獲取專業資格，為未來打算。

                只須在中六考試中考取得五科及格包括英文(課程乙)、中文及數學便可從事海上工作。在許多國家，例如英國，船員資格證書相等於大學學位。如果你取得這資格，找工作就很容易了。
              </div>
            </li>

            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">完成航海相關課程後起薪點會是多少？</a>
              <div class="accordion-content" data-tab-content>
                完成航海相關課程後，甲板部學生須作為見習生在船上的甲板上工作18個月，而學生月薪大約 HKD10,000（包括政府航海訓練獎勵計劃中的HKD5,000補助金）。
                當學生考獲第一張合格證明書(COC)後，每月工資將會有大約 HKD30,000。
                輪機部學生須作為見習生在船上工作18個月，而學生月薪大約 HKD10,000（包括政府航海訓練獎勵計劃中的HKD5,000補助金）。
                當學生考獲第一張合格證明書(COC)後，每月工資將會有大約 HKD30,000。
              </div>
            </li>

            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我取得船長證書或輪機長證書，會有哪些工作機會？</a>
              <div class="accordion-content" data-tab-content>
                工作機會很多，例如海事處需要很多取得船長證書或輪機長證書的人士出任海事主任或測量員，
                領港協會則需要取得船長證書的人士，船運公司或貨櫃碼頭營運經理須由有海事經驗人士出任，
                即使是航運物流公司都需要有海事經驗的人才。
              </div>
            </li>

            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">要取得船長證書或輪機長證書，需時多久？</a>
              <div class="accordion-content" data-tab-content>
                一般而言，中五畢業生要取得船長證書或輪機長證書需要八年時間。一個年輕人用兩年時間於海員訓練中心取得海事學文憑，
                待完成兩年海事訓練後參加三級證書考試，然後出任三副一年，再參加二級證書考試，出任二副兩年後，參加一級口試便能取得船長證書。
                但如果有機會出任大副，可獲豁免一年海事經驗，即可於一年內參加一級考試。
              </div>
            </li>

            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我不會游泳，可在海上工作嗎？</a>
              <div class="accordion-content" data-tab-content>
                如果你不會游泳，不要緊，因為你要學習的是如何在海上導航及所有相關科目。
                只有一門名為海上求生的補充科目要求你在穿著救生衣的情況下游泳。
              </div>
            </li>

            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我是女性，可在海上工作嗎？</a>
              <div class="accordion-content" data-tab-content>
                於 2003 年 12 月香港第一個女甲板練習員已經飛往澳洲到船上工作 。
                在歐洲國家，很多甲板部伙長甚至輪機員都是女性。一些台灣公司，如萬海航運或立榮海航，
                亦有許多女甲板部伙長。
              </div>
            </li>

            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我患有近視，可在海上工作嗎？</a>
              <div class="accordion-content" data-tab-content>
                只要你通過註冊醫生安排的視力測試，患有輕微近視是沒問題的，但色盲卻不能在海上工作。
                不過，患有近視的年輕人可嘗試投考輪機部，因為輛機部對入行人士的視力並無特別要求。
              </div>
            </li>
            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果家父母不准許我從事海上工作，怎麼辦？</a>
              <div class="accordion-content" data-tab-content>
                你可向父母解釋行情，告訴他們真正的海事工作與他們理解的實際上截然不同。你可請父母和你一起參觀海員訓練中心，進一步了解海上生活。海員訓練中心會不時舉辦展覽，請參看本網站提供的電話號碼，致電海員訓中心查詢。
              </div>
            </li>
            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">加入 HDMS 需要什麼資格？</a>
              <div class="accordion-content" data-tab-content>
                加入  HDMS  的最低要度是中六考試五科及格，包括英文(課程乙)、中文及數學，申請人須通過視力測驗、色盲測驗及身體檢查。
              </div>
            </li>
            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我中學會考英文科不及格，怎麼辦？</a>
              <div class="accordion-content" data-tab-content>
                對不起，英文在海上是一種重要語言，海上所有人都以英文溝通。中學會考英文科不及格者可申請重考。
              </div>
            </li>
            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我未能找到海事工作，怎麼辦？</a>
              <div class="accordion-content" data-tab-content>
                請與我們聯絡。我們與船運公司和船東協會建立了良好關係，可替你尋找工作機會。
              </div>
            </li>
            <li class="accordion-item" data-accordion-item>
              <a href="#" class="accordion-title">如果我想在海上工作，該怎麼做？</a>
              <div class="accordion-content" data-tab-content>
                你可發電郵給我們查詢詳情。基本上你須報讀海事訓練院校  HDMS  出海前課程或香港理工大學航運及物流學位，這樣你便可修讀所有海事必修科目和補充課程，如海上生活安全課程、防火課程等。
              </div>
            </li>


          </ul>
          <p>
          如果還有對海上工作有其他疑問，歡迎與我們聯絡。
          </p>
        </div>
      </div>
    );
  }
}
