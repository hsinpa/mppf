import React from "react";
import HomeAboutUs from "./HomeAboutUs";
import HomeBanner from "./HomeBanner";
import JobIntro from "./JobIntro";
import VideoIntro from "./VideoIntro";
import Story from "./Story";
import ContactUs from "./ContactUs";
import{ Element  } from 'react-scroll';

export default class Home extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-home">
        <HomeBanner />
          <div class="breakline"></div> 
        <Element name="jobIntro" className="element">
          <JobIntro id="mppf-home-jobIntro"/>
        </Element>

          <div class="breakline"></div>

        <Element name="recentNews" className="element">
          <VideoIntro id="mppf-home-videoIntro"/>
        </Element>
          <div class="breakline"></div>

        <Element name="naviStories" className="element">
          <Story id="mppf-home-story"/>
        </Element>

        <div class="breakline"></div> 
        <HomeAboutUs id="mppf-home-aboutUs"/>

        <Element name="contactUs" className="element">
          <ContactUs />
        </Element>

      </div>
    );
  }
}
