import React from "react";

export default class Story extends React.Component {
  render() {
    return (
      <div>
        <div class="callout large" id="mppf-home-contactus">
          <h2>聯絡我們</h2>
          <div class="row">
            <div class="medium-6 columns">
              <p>如你想到船上工作並加入航運業或對以下事項有任何疑問：<br/>
              想從事航運業並在海上工作，該怎樣做？<br/>
              向船運公司找工作時感到困難；<br/>
              想在訓練期間取得獎學金；<br/>
              想知道更多有關航運業行情或就業前景；</p>

              <p>歡迎透過下列方法聯絡我們：<br/>
              電話： 2544 5238 <br/>
              電郵： info.mppf@gmail.com<br/>
              </p>
            </div>
            <div class="medium-6 columns">
              <p>
              有意從事航運業並在海上工作人士，須接受出海前訓練，於海員訓練中心取得海事學高級文憑或於香港理工大學取得船務及物流業學位。<br/>

              有意取得工程資格人士，須獲取有關課程的學歷，並接受出海前訓練。<br/>

              有關入學要求及各部門的晉升途徑詳情，請參考本網站相關部分。<br/>
              海事訓練院校：<br/>
              </p>

              <p class="medium-6 columns">
              

              海事訓練學院<br/>

              新界屯門大欖涌青山公路 23號<br/>
              電話： 2458 3833<br/>
              傳真： 2440 0308<br/>
              電郵： stc@vtc.edu.hk<br/>

              </p>

              <p class="medium-6 columns">
              香港理工大學<br/>

              九龍紅磡<br/>
              電話： 2766 7418<br/>
              傳真： 2330 2704<br/>
              電郵： lgtjng@polyu.edu.hk<br/>
              </p>
            </div>
          </div>
          <form class="row" data-equalizer data-equalize-on="medium" method="post" action="/email">
            <section class="medium-6 columns" data-equalizer-watch>
              <label>姓名
                <input type="text" name="email_name"/>
              </label>
              <label>電子郵件
                <input type="email" name="email_email" />
              </label>
              <label>聯絡電話
                <input type="number" name="email_number"/>
              </label>
            </section>
            <textarea styles="margin-top: 10px;" class="medium-6 columns" rows="8" placeholder="請給我們留言" name="email_message" data-equalizer-watch></textarea>
            <input type="submit"></input>
            </form>
          </div>
      </div>
    );
  }
}
