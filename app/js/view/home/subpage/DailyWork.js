import React from "react";

export default class JobIntroDailyWork extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-home-life">





<div class="row">
        <div class="columes">
<td width="714"><p><strong>海員日常工作</strong></p>
	<br/>	<p>水手長在接收伙食<br/><img src="image/jobintro/P9-001.jpg"/></p>
	<p>船員準備放救生艇<br/><img src="image/jobintro/P9-002.jpg"/></p>
	<p>穿著呼吸器及消防員裝備準備火警演習<br/><img src="image/jobintro/P9-003.jpg"/></p>
	<p>甲板見習生在駕駛台值班<br/><img src="image/jobintro/P9-004.jpg"/></p>
	<p>泊碼頭<br/><img src="image/jobintro/P9-005.jpg"/></p>
	<p>拋撇纜<br/><img src="image/jobintro/P10-001.jpg"/></p>
	<p>船員在維修駕駛台儀器<br/><img src="image/jobintro/P10-002.jpg"/></p>
	<p>向海員講解如何使用滅火器<br/><img src="image/jobintro/P10-003.jpg"/></p>
	<p>甲板見習生在量度淡水倉<br/><img src="image/jobintro/P10-004.jpg"/></p>
	<p>船員在清潔貨倉<br/><img src="image/jobintro/P10-005.jpg"/></p>
	<p>船員用高壓氣槍在清理倉蓋路軌<br/><img src="image/jobintro/P11-001.jpg"/></p>
	<p>領港軟梯保養<br/><img src="image/jobintro/P11-002.jpg"/></p>
	<p>直昇機接送領港員上落船<br/><img src="image/jobintro/P11-003.jpg"/></p>
	<p>船員在調較主機廢氣喉氣閥<br/><img src="image/jobintro/P11-004.jpg"/></p>
	<p>船員在準備起動發電機<br/><img src="image/jobintro/P11-005.jpg"/></p>
	
    </td>



</div>
</div> 
         </div>
    );
  }
}