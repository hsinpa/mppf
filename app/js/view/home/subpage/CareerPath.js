import React from "react";

export default class JobIntroCareerPath extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-home-life">



<div class="row">
        <div class="columes">

<td width="714"><p><strong>就業前景</strong></p>
      <p>現時航運界及物流界對資深及有經驗的船長和輪機長需求甄切，原因很簡單，過去十多年間只有少數年青人出海投身航運界，導致行內人材供求失去平衡，令行內管理層平均年齡已界55 歲左右，不久將來以下的行業將求材若渴，現在正是年青人入行的最好時機！</p>
      <p><strong><a href="../chi/default.asp?deckofficer">取得船長資格證書的甲板高級船員</a>, 如果想在岸上找工作, 可以選擇以下的行業：</strong></p>
      <ul type="disc">
        <li>駐港業務船長/ 總船長 </li>
        <li>船舶代理經理 </li>
        <li>營運經理 </li>
        <li>貨物總監 </li>
        <li>貨櫃碼頭總管 </li>
        <li>驗貨師 </li>
        <li>領港員 </li>
        <li>香港特別行政區海事主任 </li>
        <li>紀律部隊高級人員，即水警警官或海關關長 </li>
        <li>海事顧問 </li>
	<li>海關和海關官員</li>
      </ul>
      <p>取得船長證書的人   如果想到岸上找工作非常容易, 原因香港從開埠到現在幾百年都是以水深港闊聞名，本港的船公司、貨櫃碼頭、物流公司及各相關行業林林種種，不下數百家。單以海事處為例都需求合資格及經驗的人幫助管理港口，而且需求非常甄切，所有興趣入航運界的年青人應該把握幾會加人航運界爭取船上的資歷及經驗，將來成為一個國際牲的專業人士。</p>
      <p><strong><a href="../chi/default.asp?engineer">取得輪機長資格證書的工程人員</a>, 如果想到岸上找工作, 可以選擇以下的行業：</strong></p>
      <ul type="disc">
        <li>海事總監 </li>
        <li>船舶經理 </li>
        <li>註冊工程師 </li>
        <li>驗船師 </li>
        <li>海事處海事驗船師 </li>
        <li>紀律部隊高級人員，即水警警司或海關關長 </li>
        <li>海事顧問 </li>
      </ul>
      <p>取得輪機長資格證書的人 如果想到岸上找工作非常容易, 原因香港從開埠到現在幾百年都是以水深港闊聞名，本港的船公司、貨櫃碼頭、物流公司及各相關行業林林種種，不下數百家。單以海事處為例都需求合資格及經驗的人幫助管理港口，很多船公司及驗船公司都需要年青的輪機長幫助管理他們的船以及驗船，而且需求非常甄切，所有興趣入航運界的年青人應該把握幾會加人航運界爭取船上的資歷及經驗，將來成為一個國際牲的專業人士。</p>
      <p>&nbsp;</p>
      </td>


      </div>
       </div> 
         </div>
    );
  }
}