import React from "react";

export default class JobIntroSeaLife extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-home-life">


<div class="row">
        <div class="columes">

      <td width="714"><p><strong>海上生活</strong></p>
      <p>當你到海上工作時你的人生將會變得多姿多彩，而你的生活模式也會變得與別不同。   到時你將會變成一個真正國際級的專業人仕，每天與不同國籍的人工作及一起生活。</p>
      <p align="center"><img id="_x0000_i1033" height="359" src="image/jobintro/lifeatsea01.gif" width="480"/></p>
      <p>作為一個 船上工作者，你將會 到海外工作 6 至 8 個月，然後有大約 2 個月假期，你將會學做一個管理人員，學習怎樣管理裝卸貨物、船上船員，以及安排船上一切工作。同時你也可以認識很多不 同國籍的人，令你   人生變得多姿多彩。</p>
      <p><strong>船上 生活模式</strong></p>
      <p><img height="240" hspace="8" src="image/jobintro/DSC00796.jpg" width="320" align="left"/>每年最少有一至二次大約二個月的大假，這樣你可以與家人到處旅遊、每天與家人相處、接送孩子上落課及做一切你以往沒有時間做的東西。   之後再集中精力到船上工作，生活既充實又多姿多彩。</p>
      <p>還有你到時將會 變 成一個真正的國際級的專業人仕，乘坐飛機變成你日常生活的一部分，因每次都可能要乘坐飛機到外地工作。 </p>
      <p align="left">在船上工作你用錢的機會很少，因為你不用交水電費。你只需執拾自己的房間及清洗自己的衣服，而其他一切食及住都是免費的。平日你可以到船上的餐廳進食自助餐。在船上你一定會學會與人相處，因為船上不管你喜不喜歡某人你也一定要與他共同生活及工作，所以你一定可以學會做人處事之道及與不同國籍的同事相處。</p>
      <p><strong>船上工作的模式</strong></p>
      <p><strong><img id="_x0000_i1076" height="180" hspace="8" src="image/jobintro/lifeatsea03.jpg" width="240" align="right" name="_x0000_i1076"/></strong>在船上工作跟一般陸上工作一樣一天正常工作 8 小時，視乎船在海上航行還是停泊碼頭，當装卸貨物時便要工作 12 小時。 例如船上三副除一天當   早晚8-12時更之外還要保養救火及逃生工具，二副便要負責航海儀器以及好像船上的醫生一樣負責船上的醫療器材，而大副便負責船上一切裝卸貨物及一切保養エ作。   船長作為船上的總指揮便要負責船上一切船員、工作、航行安全，直接向公司匯報。</p>
      
      </td>

</div>
</div> 
 </div>
    );
  }
}