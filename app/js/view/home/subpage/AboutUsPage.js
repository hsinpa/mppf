import React from "react";

export default class AboutUsPage extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-about-us-page">
        <h1>關於我們</h1>

        <div class="row">
          <div class="columes">
            <p align="justify"><strong>海 事 專 才 推 廣 聯 盟 - Maritime Professional Promotion   Federation (MPPF) </strong>旨在促進航海事業及推薦航海專才，成員包括下列機構： </p>
            <ul>
            <li>
            <p align="justify">香港航業海員合併工會 </p>
            </li>
            <li>
            <p align="justify">香港領港會有限公司 </p>
            </li>
            <li>
            <p align="justify">香港海員工會 </p>
            </li>
            <li>
            <p align="justify">香港理工學院航海系舊生會 </p>
            </li>
            <li>
            <p align="justify">香港商船高級船員協會 </p>
            </li>
            </ul>
            <p align="justify">及其他個別成員，他們來自： </p>
            <ul>
            <li>
            <p align="justify">香港理工大學 </p>
            </li>
            <li>
            <p align="justify">香港特別行政區海事處 </p>
            </li>
            <li>
            <p align="justify">海事訓練學院 </p>
            </li>
            <li>
            <p align="justify">海事學會(香港分會) </p>
            </li>
            </ul>
            <p align="justify">聯盟創辦會議於2002年7月31日舉行，海事處處長親自出席。與會人士對香港缺乏岸上海事專才配合未來港口發展的情況，均表示關注，因而一致認為應該成立聯盟，以行動促進航海事業。第二次會議於2002年9月19日舉行，聯盟的架構得以完成，並由委任成員展開籌備工作。 </p>
            <p align="justify"><strong>背景</strong></p>
            <p align="justify">鑒於本地航運業界關注到在可見的未來本地有經驗的海員及輪機員將嚴重短缺，因此有必要鼓勵多些香港的年輕人到海上去工作，並取得所需知識和資格，在未來數年內擔當業內各管理職位。 </p>
            <p align="justify">香港行政長官在最新的施政報告中，表示政府正打算將香港發展成珠江三角洲的物流及貨物轉運中心。為迎接這個挑戰，我們需要合資格的海事專才出任業內不同職務，但實情的另一面是，由於航運業在90年代缺乏政府支持，訓練學校因而在收生及維持學生數目方面遇到很大困難，課程難以續辦，以致香港理工學院出海前訓練課程於1990年被迫停辦。因此，越來越少人認識這門行業及其光明前景。 </p>
            <p align="justify">隨著大量現任岸上海事專才、高級管理人員和工程人員漸入退休之齡，問題更趨嚴重。由於一個人必須完成至少五年至八年的強制性海事服務，始能有資格獲得評核，取得船長證書或輪機長證書，因而合資格的經驗航海員的數目快將不足以填補岸上海事專才的職位空缺，如海事主任、海事測量員、領航員、船運公司及碼頭經理，尚有業內其他職務。我們必須採取及時的、有效的補救行動招募新血。 </p>
            <p align="justify">隨著失業率創下紀錄新高，航海業的工資和前景必定更具吸引力，出海工作對那些僅求一份安定工作的人似乎是大有可為的另一選擇。現在正是復興這個被稱為日漸萎縮的行業並將之帶回香港的時候了。 </p>
            <p align="justify"><strong>發展</strong></p>
            <p align="justify">在所有會員的協助下，我們發現很多學生出海回來後都極度渴望能參加一些為了合格證明書考試(COC)而提供的準備課程，以幫助他們考取好的成績。有見及此，海事專才推廣聯盟現除了推廣航海專業發展外，更擴展我們的目標和工作範圍，以支持和鼓勵海事培訓和教育，以促進香港航運專業人才的發展。</p>
            <p align="justify">2006年，我們開始於網頁上提供有關考試的相關資訊及考材，令學生能更容易得到相關資訊及往日試題作練習，以準備合格證明書考試(COC)。同年，我們亦開始第一堂專為合格證明書考試(COC)Class 3而設的預備班。我們慶幸同學覺得相關課程非常實用，亦令學生合格率大幅上升。</p>
            <p align="justify">2010年，學生已發展至需要應考合格證明書考試(COC)Class 2，所以我們亦開始開辦相關課程。同年，香港政府提供更多資助及資源於海事培訓和教育，因而令海事及航運圖書館能於香港海員工會開幕。</p>
            <p align="justify">2011年8月10日，海事專才推廣聯盟獲確認為非牟利的社會機構。因而可以開設銀行帳戶令操作更加順暢，更好地實現我們的目標。</p>
            <p align="justify"><strong>目標</strong></p>
            <p align="justify">海事專才推廣聯盟旨在於年輕人當中推廣航海事業，而長遠目標則是組織一班合資格在行海事管理專才，在香港特別行政區中奠定備受尊重的社會地位。</p>
            <p align="justify">我們會致力安排一連串推廣運動，讓香港的年輕人進一步認識航運業並最終選擇從事這行業。</p>
            <p align="justify">我們會尋求業界捐款和政府資助，為處於訓練初期的海事人員或工程師提供財政援助。</p>
            <p align="justify">海事專才推廣聯盟乃非牟利的組織，其成員除提供財政、資源及人力上的支援外，還義務參與行動以達到上述目標。</p>
            <p align="justify">我們支持並鼓勵海事專業的發展及教育，從而促進海事專業的發展。</p>
            <p align="justify"><strong>工作範圍</strong></p>
            <p align="justify">以下是聯盟活動的主要範圍和性質： </p>
            <ul>
            <li>
            <p align="justify">建設網站，為公眾人士提供有關航海業的資訊； </p>
            </li>
            <li>
            <p align="justify">為中學就業輔導教師舉辦講座； </p>
            </li>
            <li>
            <p align="justify">為中學離校人士舉辦就業講座； </p>
            </li>
            <li>
            <p align="justify">印製並派發小冊子； </p>
            </li>
            <li>
            <p align="justify">於傳媒推出廣告； </p>
            </li>
            <li>
            <p align="justify">舉行或參與職業展覽； </p>
            </li>
            <li>
            <p align="justify">為訓練期間需要財政援助的人士提供獎學金； </p>
            </li>
            <li>
            <p align="justify">接觸船運公司，為甲板部及輪機部的年輕訓練員爭取工作機會以汲取足夠航海經驗。 </p>
            </li>
            <li>
            <p align="justify">為有志於航海事業發展的同學提供航海資訊及提供不同的預備課程，為同學將來的合格證明書考試(COC)作準備。</p>
            </li>
            </ul>
            <p align="justify"></p>
            </div>
          </div>
        </div>
    );
  }
}
