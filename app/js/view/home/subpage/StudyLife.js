import React from "react";

export default class JobIntroStudyLife extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-home-life">







<div class="row">
        <div class="columes">
<td width="714">
      <p><strong>校園及船上學習生活</strong></p>
      <p align="justify">行船是一份很有前途和挑戰性的工作。學員在學校除了修讀一般常規的科目，也要修讀一些專業科目；例如航海學、航線計劃、船舶穩定、載貨方法及運算等。還有很多其他有趣而實用的知識，如船藝、繩藝、救火、海上逃生及救生艇操練等。 
      <br/></p>
      <p align="justify">為要當船長前作好準備，學員於完成相關科程後要上船做大約18 個月的實習生。期間他們要在船上工作及生活；學習跟水手們在甲板上工作，例如除鐵銹、油漆、繩藝、拉纜靠泊碼頭、泵載水及量水位等。他們還要到機房學習機房部的運作。可說是能文能武，對一些不想核板式工作及生活的人尤為合適。
      <br/></p>
      <p align="justify">除此以上，最重是要到駕駛台學習操控船隻、管理及修改海圖、定船位、找羅經誤差、運用各種航海儀器，如電子海圖、衛星定位儀、雷達及其他先進電子航海儀器；現在海上航行已經比以前很安全及先進。還有學員要學習海上的規矩；船上的階級觀念很重要，學員要穿著制服、學習旗號、好像海軍一樣的禮儀和規格。</p>
      
      <p><strong></strong>&nbsp;</p>
      
      <p><strong>繩藝</strong></p>
      <p align="center">
      <img id="_x0000_i1033" align="left"  src="image/jobintro/lifeatschool01.jpg"width="180" height="240"/>
      學習船上水手工藝，如繩吊椅及吊板。
      </p>

      <p><strong>船塢</strong></p>
      <p align="center">
      <img id="_x0000_i1033" align="left" src="image/jobintro/lifeatschool02.jpg" width="180" height="240"/>
      有機會參觀船塢
      </p>
      
      <p><strong>六分儀</strong></p>
      <p align="center">
      <img id="_x0000_i1034" align="left" src="image/jobintro/lifeatschool03.jpg" width="180" height="240"/> 
      使用六分儀定位 
      </p>

        
      <p><strong>差繩纜</strong> </p>
      <p align="center">
      <img id="_x0000_i1033" align="left" src="image/jobintro/lifeatschool04.jpg" width="180" height="240"/>
      學習結繩纜</p>
      
      <p><strong>消防訓練演習</strong></p>
	  <p><img src="image/jobintro/001.jpg"/></p>
    <p><img src="image/jobintro/002.jpg"/></p>
	  <p><img src="image/jobintro/003.jpg"/></p>
	  <p><img src="image/jobintro/004.jpg"/></p>
	
      <p><strong>跳水訓練</strong></p>
	  <p><img src="image/jobintro/005.jpg"/></p>
	 
      <p><strong>救生筏演習</strong></p>
	  <p><img src="image/jobintro/006.jpg"/></p>
	  </td>


</div>
</div> 
      </div>
    );
  }
}