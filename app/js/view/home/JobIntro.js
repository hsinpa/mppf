import React from "react";
import { Link, Route } from 'react-router';

export default class JobIntro extends React.Component {
  render() {
    return (
      <div id="mppf-home-jobIntro">
          <h2>旅程開始</h2>
          <div class="row">

              <div class="medium-3 columns" >
                <div class="callout" >
                   <Link to="/jobIntro/StudyLife" class="know-more"><img src= "image/test/j1.png" /></Link>
                    <h6>校園及船上學習生活</h6>
                    <p class="job-intro-desc-text">行船是一份很有前途和挑戰性的工作。學員在學校要收讀一些專業科目。</p>

                    <div class="medium-12 columns text-center">
                      <Link to="jobIntro/StudyLife" class="know-more"><a href="#" class="button radius">了解更多</a></Link>
                    </div>
                </div>
              </div>

              <div class="medium-3 columns" >
                <div class="callout" >
                   <Link to="/jobIntro/SeaLife" class="know-more"><img src= "image/test/j2.png" /></Link>
                    <h6>海上生活</h6>
                    <p class="job-intro-desc-text">當你到海上工作時你的人生將會變得多姿多彩，而你的生活模式也會變得與別不同。 你將會變成一個真正國際級的專業人仕，每天與不同國籍的人工作及一起生活。</p>

                    <div class="medium-12 columns text-center">
                      <Link to="/jobIntro/SeaLife" class="know-more"><a href="#" class="button radius">了解更多</a></Link>
                    </div>
                </div>
              </div>

              <div class="medium-3 columns" >
                <div class="callout" >
                  <Link to="/jobIntro/DailyWork" class="know-more"><img src= "image/test/j3.png" /></Link>
                    <h6>海員日常工作</h6>

                  <p class="job-intro-desc-text">每個職級亦有不過的工作崗位，各施其職，合作無間。</p>

                  <div class="medium-12 columns text-center">
                      <Link to="/jobIntro/DailyWork" class="know-more"><a href="#" class="button radius">了解更多</a></Link>
                    </div>
                </div>
              </div>
              <div class="medium-3 columns" >
                <div class="callout" >
                  <Link to="/jobIntro/CareerPath" class="know-more"><img src= "image/test/j4.png" /></Link>
                    <h6>就業前景</h6>

                    <p class="job-intro-desc-text">現時航運界及物流界對資深及有經驗的船長和輪機長需求甄切，現在正是年青人入行的最好時機！</p>

                    <div class="medium-12 columns text-center">
                      <Link to="/jobIntro/CareerPath" class="know-more"><a href="#" class="button radius">了解更多</a></Link>
                    </div>

                </div>
              </div>
            </div>
      </div>
    );
  }
}
