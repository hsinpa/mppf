import React from "react";
import NewsFeed from "../components/NewsFeed";

export default class VideoIntro extends React.Component {
  render() {
    return (
      <div class="row">
        <iframe id="home-video" frameBorder="0" height="534" class="medium-8 columns"
                src="https://www.youtube.com/embed/kI4-qwsUoxM" allowfullscreen>
        </iframe>

        <div class="medium-4 columns">
          <NewsFeed/>
        </div>
      </div>
    );
  }
}
