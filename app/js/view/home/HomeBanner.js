import React from "react";
import{ Element  } from 'react-scroll';

export default class HomeBanner extends React.Component {
  render() {
    return (
      <div class="row">
                <div class="rslides_container medium-12 columns">
        <ul class="rslides">
          <li><img src="image/home/b1.jpg"></img></li>
          <li><img src="image/home/b2.jpg"></img></li>
          <li><img src="image/home/b3.jpg"></img></li>
        </ul>
      </div>
        <Element name="aboutUs" className="element">
        
        <p class="medium-12 columns" id="headline-text">
          現在航海是一份既安全又先進的專業，不單待遇優厚而且前途無限。請加入我們，做個未來的船長或輪機長吧！你願意接受這個挑戰嗎？航運業提供一系列就業機會 ─ 無論是陸上還是海上，工資優厚、有趣、富挑戰性，從電子到工程、航海、法律、會計、造船學、保險及物流，就業機會十分廣泛。
        </p>
      </Element>
      </div>
    );
  }
}
