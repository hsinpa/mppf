import React from "react";
import { Link, Route } from 'react-router';

export default class Story extends React.Component {
    render() {
        return (
            <div id="mppf-home-story">
                <h2>航海故事</h2>

                <div class="row">
                    <div class="medium-3 columns">

                        <div class="story-card">
                            <Link to="/story/Carmen">
                                <img src="image/people/carmen.jpg"/>
                            </Link>
                            <div class="description">
                                <h5>陳嘉敏船長</h5>
                                <p>甲板伙長<br/>航海經驗： 2003 至 2014<br/>現職海事處海事主任</p>
                            </div>
                        </div>

                    </div>

                    <div class="medium-3 columns">

                        <div class="story-card">
                            <Link to="/story/Lam_Ming_Fung">
                                <img src="image/people/Lam_Ming_Fung_sq.jpg"/>
                            </Link>
                            <div class="description">
                                <h5>林銘鋒船長</h5>
                                <p>M.V.Cape Shanghai 船長<br/>航海經驗：1998至今<br/>現職船公司副總裁</p>
                            </div>
                        </div>

                    </div>

                    <div class="medium-3 columns">

                        <div class="story-card">
                            <Link to="/story/lyl">
                                <img src="image/people/Matt_sq.jpg"/>
                            </Link>
                            <div class="description">
                                <h5>黎永麟船長</h5>
                                <p>甲板伙長<br/>航海經驗： 2003 至今<br/>現職領航員</p>
                            </div>
                        </div>

                    </div>

                    <div class="medium-3 columns">

                        <div class="story-card">
                            <Link to="/story/Matthew">
                                <img src="image/people/Matthew_sq.png"/>
                            </Link>
                            <div class="description">
                                <h5>蕭邦泰(輪機) </h5>
                                <p>航海經驗： 2006 至2012<br/>現職海事處驗船主任(輪機) </p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        );
    }
}
