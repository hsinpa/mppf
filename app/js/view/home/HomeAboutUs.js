import React from "react";
import{ Element  } from 'react-scroll';
import { Link, Route } from 'react-router';

export default class HomeAboutUs extends React.Component {

  componentDidMount() {
    $(".rslides").responsiveSlides({
      auto: true,
      pager: true,
      nav: true,
      speed: 500,
      timeout: 4000,
      namespace: "transparent-btns"
    });
  }

  render() {
    return (
      <div class="row">
        <Element name="aboutUs" className="element">
        <h2>關於我們</h2>
        <p class="medium-12 columns">
          海事專才推廣聯盟 - Maritime Professional Promotion Federation (MPPF) 旨在促進航海事業及推薦航海專才。
          <br/>海事專才推廣聯盟旨在於年輕人當中推廣航海事業，而長遠目標則是組織一班合資格在行海事管理專才，在香港特別行政區中奠定備受尊重的社會地位。
          <br/>我們會致力安排一連串推廣運動，讓香港的年輕人進一步認識航運業並最終選擇從事這行業。
          <br/>我們會尋求業界捐款和政府資助，為處於訓練初期的海事人員或工程師提供財政援助。
          <br/>海事專才推廣聯盟乃非牟利的組織，其成員除提供財政、資源及人力上的支援外，還義務參與行動以達到上述目標。
          <br/>我們支持並鼓勵海事專業的發展及教育，從而促進海事專業的發展。
          <br/>
          
        </p>
        <div class="medium-12 columns text-center">
        <Link to="/aboutUsPage" class="know-more"><a href="#" class="button radius">了解更多</a></Link>
        </div>
        
      </Element>
      </div>
    );
  }
}
