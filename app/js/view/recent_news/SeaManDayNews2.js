import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class SeaManDayNews2 extends React.Component {
  render() {
    return (
    	<div id="mppf-recentNews-seaManDay2" class="row">
	    	<div class="medium-8 columns">
	    	<NewsHeaderLayout title="6.25 海員日" lastUpdate="25-2-2016"/>
				<p>國際海事組織在2010年6月於菲律賓馬尼拉舉行的《1978年海員培訓、發證和值班標準國際公約》締約國外交大會上通過一項決議, 訂定每年6月25日為“海員日”，感謝海員往往在他們自己和家人的巨大付出下，為國際貿易和世界經濟作出無價的貢獻。</p>
				<p>“海員日” 的首次慶祝是在2011年，各國政府，航運組織，企業，船東和所有其他相關團體均以適當和有意義的方式慶祝並推動此日子。而”海員日” 更已被列入聯合國紀念活動年度清單。</p>
				<img src="image/story/s009.jpg"/>
				<img src="image/story/s006.jpg"/>
			</div>
		
			<div class="medium-4 columns">
	            <NewsFeed/>
	        </div>
        </div>
	    );
  }
}