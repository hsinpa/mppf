import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class SeaManDayNews extends React.Component {
  render() {
    return (
    	<div id="mppf-recentNews-seaManDay" class="row">
	    	<div class="medium-8 columns">
	    	<NewsHeaderLayout title="「海員日」感謝無價貢獻" lastUpdate="25-2-2016"/>
				<p><strong><a name="Top" id="Top"></a>「海員日」感謝無價貢獻</strong></p>
			    <p>為慶祝國際海事組織訂定6月25日為「海員日」，海事處、香港海員工會及香港航運界聯席會議在香港合辦一連串活動，令公眾人士了解海員的貢獻，並向他們致敬。</p>
				<p>聯合國國際海事組織於2010年把每年的6月25日訂為「海員日」，藉此對全球約150萬名為國際貿易和世界經濟作出無價貢獻的海員表達謝意。</p>
				<p>在香港，慶祝「海員日」的首項活動「海員生活圖片展」將於6月16日(星期六)舉行，展示海員在船上工作和生活的照片，並由一群年青海員擔任講解員，向參觀人士講解海員的實際工作和生活情況。市民在6月16日下午1時至下午5時到銅鑼灣行人專用區，或在6月17日(星期日) 下午1時至下午5時到旺角行人專用區參觀巡迴展覽。</p>
				<p><img src="image/story/IMG_0891.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_0912.jpg"/></p>
				<p><img src="image/story/IMG_1772.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_1780.jpg"/></p>
				<p><img src="image/story/IMG_1782.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_1790.jpg"/></p>
				<p><img src="image/story/IMG_5467.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_5519.jpg"/></p>
				<p><img src="image/story/IMG_6414.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_6454.jpg"/></p>
				<p><img src="image/story/IMG_6469.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_6471.jpg"/></p>
				<p><img src="image/story/IMG_6504.jpg"/></p>
				<p>在6月22日(星期五)，合辦機構將安排修讀航海課程的學生參觀葵青貨櫃碼頭，認識港口運作，以及登上貨櫃船，讓學生親身了解船員的工作及生活。</p>
				<p><img src="image/story/IMG_6696.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_0976.jpg"/></p>
				<p><img src="image/story/IMG_6613.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_1014.jpg"/></p>
				<p><img src="image/story/IMG_1019.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_1042.jpg"/></p>
				<p><img src="image/story/IMG_1064.jpg"/></p>
				<p>合辦機構於6月25日(星期一)在尖沙咀海員俱樂部舉行「海員日」自助餐晚宴，讓航運業界人士及修讀航海課程的學生一同慶祝這個別具意義的日子，海事處處長廖漢波亦有參與。</p>
				<p><img src="image/story/DSC09231.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/DSC09217.jpg"/></p>
				<p><img src="image/story/DSC09243.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/DSC09327.jpg"/></p>
				<p><img src="image/story/DSC09383.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/DSC09637.jpg"/></p>
				<p><img src="image/story/IMG_5642.jpg"/>&nbsp;&nbsp;&nbsp;<img src="image/story/IMG_5655.jpg"/></p>
			</div>
		
			<div class="medium-4 columns">
	            <NewsFeed/>
	        </div>
        </div>
	    );
  }
}