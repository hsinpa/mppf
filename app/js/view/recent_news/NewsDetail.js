/**
 * Created by kenli on 22/5/2017.
 */
import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class NewsDetail extends React.Component {
  constructor(props) {
    super(props);
    console.log("newsId = " + this.props.params.newsId)

    this.state = {
      detail: []
    }
  }

  componentDidMount() {
    var that = this;
    var url = '/api/news/' + this.props.params.newsId;

    fetch(url)
      .then(function(response) {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then(function(data) {
        console.log(data);
        that.setState({ detail: data });
      });
  }

  render() {
    var detail = this.state.detail[0];
    if (detail !== undefined) {
      var timeStr = detail.display_datetime;
      var date = new Date(timeStr);
      var displayTimeStr = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()

      return (
        <div id="mppf-recent_news-SeaCamp">
          <div class="row">
            <div class="columes">
              <div class="medium-8 columns">
                <NewsHeaderLayout title={detail.title} lastUpdate={displayTimeStr}/>
                    <div dangerouslySetInnerHTML={this.createMarkup()} />
              </div>
              <div class="medium-4 columns">
                <NewsFeed/>
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      return (<p></p>)
    }
  }

  createMarkup() {
    return {__html: this.state.detail[0].content};
  }

}