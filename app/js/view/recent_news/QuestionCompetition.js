import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class QuestionCompetition extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-recent_news-QuestionCompetition">
        <div class="row">
            <div class="columes">
                <div class="medium-8 columns">
                    <NewsHeaderLayout title="海事專才推廣聯盟 海事常識網上問答比賽 2" lastUpdate="25-2-2016"/>
                    <td width="725" class="cnormal">


                        <p class="cnormal">
                            <strong><a href="https://www.facebook.com/events/1666420333611557/">海事專才推廣聯盟 海事常識網上問答比賽 2</a></strong>
                        </p>
                        
                        <p class="style2">
                        這個海事常識問答比賽是以中小學生為對象，範圍以「航海」為主，了解香港仍有海員這個行業。透過問答比賽，我們可以引起青少年對海事科技及航海的興趣。<br/>
                       <br/>
                       可參考相關網頁<a href="http://seagoinghk.org">http://seagoinghk.org</a><br/>
                        活動日期:由2016年2月25日至2016年4月1日(下午12時正)<br/>
                        參賽資格:全日制學生以個人身份參加<br/><br/>
                        參加方法:<a href="http://goo.gl/forms/FQ4P9mJTsQ">http://goo.gl/forms/FQ4P9mJTsQ</a><br/><br/>
                        獎金及獎狀<br/>
                        冠軍: 港幣500元書券(1位)<br/>
                        優異獎: 港幣100元書券(20位)
                        </p>
                    </td>

                </div>
                <div class="medium-4 columns">
                    <NewsFeed/>
                </div>
            </div>
        </div> 
         </div>
    );
  }
}