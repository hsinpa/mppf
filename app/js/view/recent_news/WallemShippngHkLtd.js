import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class WallemShippngHkLtd extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-recent_news-WallemShippngHkLtd">
        <div class="row">
            <div class="columes">
                <div class="medium-8 columns">
                    <NewsHeaderLayout title="參觀遠洋船公司(華林集團 Wallem shipping hk ltd)" lastUpdate="25-2-2016"/>
                    <td width="725" class="cnormal">


                        <p class="cnormal">
                            <strong><a href="https://www.facebook.com/events/451912681671258/">參觀遠洋船公司(華林集團 Wallem shipping hk ltd)</a></strong>
                        </p>
                        
                        <p class="style2">
                        活動日期：2016年3月30日(三)<br/>
                        集合時間：下午2時45分至4時正<br/>
                        集合地點：鰂魚涌太古坊<br/>
                        費用:全免<br/>
                        請於3月28日(一)2300HRS或以前填妥表格<br/>
                        <a href="http://goo.gl/forms/XHNuRJkWiy">http://goo.gl/forms/XHNuRJkWiy</a>
                        , 未能準時提交資料參觀將不獲受理。
                        </p>
                
                    </td>

                </div>
                <div class="medium-4 columns">
                    <NewsFeed/>
                </div>
            </div>
        </div> 
      </div>
    );
  }
}