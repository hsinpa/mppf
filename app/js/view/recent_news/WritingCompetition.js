import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class WallemShippngHkLtd extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-recent_news-WritingCompetition">
        <div class="row">
            <div class="columes">
                <div class="medium-8 columns">
                    <NewsHeaderLayout title="「海上的奇遇」徵文比賽" lastUpdate="25-2-2016"/>
                    <td width="725" class="cnormal">


                        <p class="cnormal">
                            <strong>「海上的奇遇」徵文比賽</strong>
                        </p>
                        
                        <p class="style2">
                        主題：以「海上的奇遇」為題，自由發揮，於文中發表個人見解、記敘難忘經歷、也可以表達渴望、書寫暢想<br/>
                        文體：敘事文、抒情文或故事創作<br/>
                        字數：1,000字或以內<br/>
                        截稿日期:至5月31日(下午12時正)<br/>
                        參賽資格:全日制學生以個人身份參加<br/>
                        獎金及獎狀:<br/>
                        冠軍: 雙魚星號雙人郵輪之旅及港幣1000元書券(1位)<br/>
                        優異獎: 港幣500元書券(5位)<br/>
                        公佈日期: 2016年6月30日前於Facebook公佈。<br/>
                        如有任何爭議,以大會公佈為準。<br/><br/>
                        報名及查詢2332 0766 陳小姐<br/>
                        </p>
                
                    </td>

                </div>
                <div class="medium-4 columns">
                    <NewsFeed/>
                </div>
            </div>
        </div> 
         </div>
    );
  }
}