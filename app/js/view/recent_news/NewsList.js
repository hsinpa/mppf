/**
 * Created by ken on 18/5/2017.
 */
import React from "react";
import {Link, Route} from 'react-router';

export default class NewsList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      news: []
    };
  }

  componentDidMount() {
    var that = this;
    var url = '/api/news'

    // fetch(url)
    //   .then(function (response) {
    //     if (response.status >= 400) {
    //       throw new Error("Bad response from server");
    //     }
    //     return response.json();
    //   })
    //   .then(function (data) {
    //     that.setState({news: data});
    //   });

    $.ajax({
      url: url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        console.log("get news data");
        that.setState({news: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  }

  render() {
    var newsViews = this.state.news.map(function (newsItem) {

      var createContent = function createContent(content) {
        return {__html: content};
      };

      var detailUrl = "/newsDetail/" + newsItem.id;

      var timeStr = newsItem.display_datetime;
      var date = new Date(timeStr);
      var displayTimeStr = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()

      return (
        <section class="column">
          <img src={newsItem.image}></img>
          <h5 class="post-title">{newsItem.short_title}</h5>
          <p dangerouslySetInnerHTML={createContent(newsItem.short_content)}/>
          <div class="row">
            <Link to={detailUrl}><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
            <a class="small-6 columns post-date">{displayTimeStr}</a>
          </div>
        </section>
      )
    });

    return (
      <div id="mppf-recentNews">
        <h1>最新消息</h1>
        <article class="row" data-equalizer data-equalize-by-row="true">
          {newsViews}
        </article>


      </div>
    )
  }


}