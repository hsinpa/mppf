import React from "react";
import { Link, Route } from 'react-router';

export default class RecentNews extends React.Component {
  render() {
    return (
      <div id="mppf-recentNews">
        <h1>最新消息</h1>
          <article class="row" data-equalizer data-equalize-by-row="true">
            <section class="column">
              <img src="image/home/IMG_5791.jpg"></img>
              <h5 class="post-title">海事運作專業文憑</h5>
              <p>
                海事訓練學院現推出兼讀制「海事運作專業文憑」(PDMO) 課程，讓學員可獲取更高學歷，來提升個人職涯發展的競爭力。
              </p>
              <div class="row">
                <Link to="recentNews/PDMO"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2016-09-17</a>
              </div>
            </section>
            <section class="column">
              <img src="image/home/IMG_5791.jpg"></img>
              <h5 class="post-title">海事常識網上問答比賽 2</h5>
              <p>
                這個海事常識問答比賽是以中小學生為對象，範圍以「航海」為主，了解香港仍有海員這個行業。透過問答比賽，我們可以引起青少年對海事科技及航海的興趣。
              </p>
              <div class="row">
                <Link to="recentNews/QuestionCompetition"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2016-02-25</a>
              </div>
            </section>
            <section class="column">
              <img src="image/home/ws.png"></img>
              <h5 class="post-title">參觀遠洋船公司</h5>
              <p>
                (華林集團 Wallem shipping hk ltd)<br/>
                 活動日期：2016年3月30日(三)<br/>集合時間：下午2時45分至4時正<br/>集合地點：鰂魚涌太古坊<br/>費用:全免  
              </p>
              <div class="row">
                <Link to="recentNews/WallemShippngHkLtd"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2016-02-25</a>
              </div>
            </section>
            <section class="column">
              <img src="image/home/chx.png"></img>
              <h5 class="post-title">「乘風航」海上歷奇營</h5>
              <p>
                活動日期:2016年5月7至8日(兩日一夜)<br/>參賽資格:16至29歲，全日制學生<br/>收費：$200 (已包括團服、保險及餐費)
              </p>
              <div class="row">
                <Link to="recentNews/SeaCamp"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2016-02-25</a>
              </div>
            </section>
            <section class="column">
              <img src="image/home/IMG_5791.jpg"></img>
              <h5 class="post-title">「海上的奇遇」徵文比賽</h5>
              <p>
                主題：以「海上的奇遇」為題，自由發揮，於文中發表個人見解、記敘難忘經歷、也可以表達渴望、書寫暢想<br/><br/>文體：敘事文、抒情文或故事創作
              </p>
              <div class="row">
                <Link to="recentNews/WritingCompetition"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2016-02-25</a>
              </div>
            </section>
            <section class="column">
              <img src="image/home/2013.jpg"></img>
              <h5 class="post-title">海員日</h5>
              <p>
               為慶祝國際海事組織訂定6月25日為「海員日」，海事處、香港海員工會及香港航運界聯席會議在香港合辦一連串活動，令公眾人士了解海員的貢獻，並向他們致敬。
              </p>
              <div class="row">
                <Link to="recentNews/seaManDay"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2012-06-25</a>
              </div>
            </section>
            <section class="column">
              <img src="image/test/banner.png"></img>
              <h5 class="post-title">海員日</h5>
              <p>
               為慶祝國際海事組織訂定6月25日為「海員日」，海事處、香港海員工會及香港航運界聯席會議在香港合辦一連串活動，令公眾人士了解海員的貢獻，並向他們致敬。
              </p>
              <div class="row">
                <Link to="recentNews/seaManDay2"><a class="small-6 columns read-more"><u>閱讀更多 ></u></a></Link>
                <a class="small-6 columns post-date">2010-06-25</a>
              </div>
            </section>
          </article>


      </div>
    );
  }
}