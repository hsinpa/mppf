import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class QuestionCompetition extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-recent_news-QuestionCompetition">
        <div class="row">
            <div class="columes">
                <div class="medium-8 columns">
                    <NewsHeaderLayout title="海事運作專業文憑" lastUpdate="17-09-2016"/>
                    <td width="725" class="cnormal">


                        <p class="cnormal">
                            海事訓練學院現推出兼讀制「海事運作專業文憑」(PDMO) 課程，讓學員可獲取更高學歷，來提升個人職涯發展的競爭力。<br/><br/>申請人若持有IVE海事科技文憑 (DMS) 或 甲板高級船員三級合格證書或同等學歷，並具一年相關工作經驗者便可以獲50% 的課程豁免。<br/><br/>詳情可參閱課程簡介：<a href="http://www.msti.edu.hk/news.php?cat=news">http://www.msti.edu.hk/news.php?cat=news</a>
                        </p>
                    </td>

                </div>
                <div class="medium-4 columns">
                    <NewsFeed/>
                </div>
            </div>
        </div> 
         </div>
    );
  }
}