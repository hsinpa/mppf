import React from "react";
import NewsFeed from "../components/NewsFeed";
import NewsHeaderLayout from "../components/NewsHeaderLayout";

export default class WallemShippngHkLtd extends React.Component {
  componentDidMount() {
    $(document).foundation();
  }

  render() {
    return (
      <div id="mppf-recent_news-SeaCamp">
        <div class="row">
            <div class="columes">
                <div class="medium-8 columns">
                    <NewsHeaderLayout title="海事專才推廣聯盟 「乘風航」海上歷奇營" lastUpdate="25-2-2016"/>
                    <td width="725" class="cnormal">


                        <p class="cnormal">
                            <strong><a href="https://www.facebook.com/events/972697256151790/">海事專才推廣聯盟 「乘風航」海上歷奇營</a></strong>
                        </p>
                        
                        <p class="style2">
                        活動日期:2016年5月7至8日(兩日一夜)<br/>
                        參賽資格:16至29歲，全日制學生<br/>
                        收費：$200 (已包括團服、保險及餐費)<br/><br/>
                        報名日期：由即日起，接受報名，名額先到先得，額滿即止。<br/>
                        報名截止：2016年4月19日(星期二)<br/>
                        報名手續：填妥表格<br/>
                        <a href="http://goo.gl/forms/7Cbax6o5Fb">http://goo.gl/forms/7Cbax6o5Fb </a> 3天內繳交團費, 名額於收款後才作確認
                        </p>
                    </td>

                </div>
                <div class="medium-4 columns">
                    <NewsFeed/>
                </div>
            </div>
        </div> 
         </div>
    );
  }
}