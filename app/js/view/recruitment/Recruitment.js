import React from "react";
import NewsFeed from "../components/NewsFeed";
import Avatar from "../components/Avatar";

export default class Recruitment extends React.Component {
  render() {

    return (
      <div id="mppf-recruitment" class="row">

          <div class="medium-8 columns">

            <form class="recruitment-search">
                <input type="text" placeholder="搜索關鍵字"></input>
                <select >
                  <option value="volvo">Volvo</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
                <input type="submit" class=""></input>
              </form>

                <ul class="mppf-recruitment-list">
                  <li class="mppf-recruitment-employee row">

                    <div class="mppf-recruitment-employee-left small-6 columns">
                      <div class="row">
                        <img src="image/test/circleIcon.png" class="medium-5 columns"></img>
                        <section class="medium-7 columns">
                          <h5>旅程開始</h5>
                          <p>現在航海是一份既安全又先記得專業,不單待遇呦厚而已前途無限．請</p>
                        </section>

                      </div>
                    </div>

                    <div class="mppf-recruitment-employee-right small-6 columns">
                      <ul>
                        <li>-時航運界及物流界對資深</li>
                        <li>-時航運界及物流界對資深</li>
                        <li>-時航運界及物流界對資深</li>
                      </ul>
                    </div>

                  </li>
                  <li class="mppf-recruitment-employee row">

                    <div class="mppf-recruitment-employee-left small-6 columns">
                      <div class="row">
                        <img src="image/test/circleIcon.png" class="medium-5 columns"></img>
                        <section class="medium-7 columns">
                          <h5>旅程開始</h5>
                          <p>現在航海是一份既安全又先記得專業,不單待遇呦厚而已前途無限．請</p>
                        </section>

                      </div>
                    </div>

                    <div class="mppf-recruitment-employee-right small-6 columns">
                      <ul>
                        <li>-時航運界及物流界對資深</li>
                        <li>-時航運界及物流界對資深</li>
                        <li>-時航運界及物流界對資深</li>
                      </ul>
                    </div>

                  </li>
                </ul>

          </div>

          <div class="medium-4 columns">
            <Avatar/>

            <NewsFeed/>
          </div>

      </div>
    );
  }
}
