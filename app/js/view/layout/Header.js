import React from "react";
import { Link, Route } from 'react-router';
import ReactDOM from "react-dom";
import{ Scroll, Link as Scroll_Link } from 'react-scroll';

export default class Header extends React.Component {
  constructor() {
      super();
      $(".off-canvas-list a").click(function () {
                $("#offCanvas").foundation('close');
      });
  }

  CreateLink(destination, name, toHomePage = false) {
    var path = this.props.route.path;

    return (path == undefined) ?<Scroll_Link  to={destination} spy={true} smooth={true} duration={500}>{name}</Scroll_Link> :
     <Link to={(toHomePage) ? "/" : destination}>{name}</Link>;
  }

  render() {

    return (
      <header class="top-bar">

        <div class="desktopHeader show-for-medium">
          <div class="row" data-equalizer data-equalize-on="medium">
            <div class="headerLogo small-1 column" data-equalizer-watch>
              <img  class="show-for-medium" src="image/test/mppf.png"></img>
            </div>

            <div class="small-9 column" data-equalizer-watch>
              <ul>
                <li><Link to="/" isHome="true">主頁</Link></li>
                <li>{this.CreateLink("aboutUs", "關於我們", true)}</li>
                <li>{this.CreateLink("jobIntro", "旅程開始", true)}</li>
                <li><Link to="naviStories">航海故事</Link></li>
                <li><Link to="recentNews">最新消息</Link></li>
                <li><Link to="resources">考試預備資源</Link></li>
                <li>{this.CreateLink("contactUs", "聯絡我們", true)}</li>
                <li><Link to="faq">常見問題</Link></li>
              </ul>
            </div>

            <div class="headerSearch small-2 column" data-equalizer-watch>
              <a href="https://www.facebook.com/seagoinghk/"><img src="image/test/fb2.png" id="find-us-on-fb"></img></a>
            </div>
          </div>
        </div>

        <div class="title-bar-left show-for-small-only">
          <button class="menu-icon" type="button" data-open="offCanvas"></button>
          <span class="title-bar-title">
            <Link to="/" ><span class="title-bar-title">主頁</span></Link>
          </span>
        </div>
      </header>
     );
  }
}
