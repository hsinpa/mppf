import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';

import Layout from "./view/layout/Layout";
import Home from "./view/home/Home";
import Recruitment from "./view/recruitment/Recruitment";
import RecruitDetail from "./view/recruitment/RecruitDetail";
import NaviStories from "./view/story/NaviStories";
import RecentNews from "./view/recent_news/RecentNews";
import FAQPage from "./view/FAQ/FAQ";
import SeaManDayNews from "./view/recent_news/SeaManDayNews";
import SeaManDayNews2 from "./view/recent_news/SeaManDayNews2";
import AboutUsPage from "./view/home/subpage/AboutUsPage";
import Resources from "./view/resources/Resources";

import Carmen from "./view/story/Carmen";
import Lam_Ming_Fung from "./view/story/Lam_Ming_Fung"
import Matthew from "./view/story/Matthew"
import lyl from "./view/story/lyl"

import StudyLife from "./view/home/subpage/StudyLife"
import SeaLife from "./view/home/subpage/SeaLife"
import DailyWork from "./view/home/subpage/DailyWork"
import CareerPath from "./view/home/subpage/CareerPath"

import WallemShippngHkLtd from "./view/recent_news/WallemShippngHkLtd"
import QuestionCompetition from "./view/recent_news/QuestionCompetition"
import SeaCamp from "./view/recent_news/SeaCamp"
import WritingCompetition from "./view/recent_news/WritingCompetition"
import PDMO from "./view/recent_news/PDMO"

import NewsList from "./view/recent_news/NewsList";
import NewsDetail from "./view/recent_news/NewsDetail";

require("../css/main/app.scss");


const app = document.getElementById('app');
ReactDOM.render((
  <Router history={hashHistory}
    onUpdate={function() {

      }}>
    <Route path="/" name="home" component={Layout}>
      <IndexRoute component={Home} ></IndexRoute>
      //Ken: Home sub page
      <Route path="/aboutUsPage" name="AboutUsPage" component={AboutUsPage}></Route>

      <Route path="recruitment" name="recruitment" component={Recruitment}></Route>
      <Route path="recruitment/:id" component={RecruitDetail}></Route>
      <Route path="recentNews"  name="recentNews" component={NewsList}></Route>
      <Route path="naviStories"  name="naviStories" component={NaviStories}></Route>
      <Route path="faq"  name="faq" component={FAQPage}></Route>
      <Route path="resources"  name="resources" component={Resources}></Route>
      <Route path="recentNews/seaManDay"  name="seaManDay" component={SeaManDayNews}></Route>

      <Route path="story/Carmen"  name="Carmen" component={Carmen}></Route>
      <Route path="story/Lam_Ming_Fung"  name="Lam_Ming_Fung" component={Lam_Ming_Fung}></Route>
      <Route path="story/Matthew"  name="Matthew" component={Matthew}></Route>
      <Route path="story/lyl"  name="lyl" component={lyl}></Route>
      
      <Route path="jobIntro/StudyLife"  name="StudyLife" component={StudyLife}></Route>
      <Route path="jobIntro/SeaLife"  name="SeaLife" component={SeaLife}></Route>
      <Route path="jobIntro/DailyWork"  name="DailyWork" component={DailyWork}></Route>
      <Route path="jobIntro/CareerPath"  name="CareerPath" component={CareerPath}></Route>
      
      <Route path="recentNews/WallemShippngHkLtd"  name="WallemShippngHkLtd" component={WallemShippngHkLtd}></Route>
      <Route path="recentNews/QuestionCompetition"  name="QuestionCompetition" component={QuestionCompetition}></Route>
      <Route path="recentNews/SeaCamp"  name="SeaCamp" component={SeaCamp}></Route>
      <Route path="recentNews/WritingCompetition"  name="WritingCompetition" component={WritingCompetition}></Route>
      <Route path="recentNews/SeaManDay2"  name="SeaManDay2" component={SeaManDayNews2}></Route>
      <Route path="recentNews/PDMO"  name="PDMO" component={PDMO}></Route>


      <Route path="newsList"  name="newsList" component={NewsList}></Route>
      <Route path="newsDetail/:newsId"  name="newsDetail" component={NewsDetail}></Route>
      
  </Route>
  </Router>
), app)
